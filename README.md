# Blog Post API

## By default login = user, password = password
## Also url is http://localhost:5001 by default, it can be changed in .env

all app parameters are located in .env

folder config contains configurations for database connection

## Install

```npm install```

## Run the app

```npm start```

## You can see documentation on http://localhost:5001/docs
