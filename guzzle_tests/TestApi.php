<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;

class TestApi {
    public $newLine = "\r\n";
    public $client;

    function __construct($idsArray, $posts, $newPosts, $port) {
        $this->client = new Client([
            'base_uri' => 'http://localhost:' . $port,
            'timeout'  => 2.0,
            'http_errors' => false
        ]);
    }

    function getPosts() {
        $res = $this->client->request('GET', '/posts', [
            'auth' => ['user', 'password']
        ]);
        $status = $res->getStatusCode();
        
        if ($status != 200) {
            echo 'Cannot get posts status code is ' . $status . $this->newLine;
        }
    }

    function getPostById($id) {
        $endpoint = '/posts/' . ($id);
        $res = $this->client->request('GET', $endpoint, [
        'auth' => ['user', 'password']]);

        $status = $res->getStatusCode();

        if ($status == 404) {
            echo 'Not found any post with this id: ' . $id . $this->newLine;
        }
    }

    function createPost($post) {
        $res = $this->client->post('/posts', [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($post),
            'auth' => ['user', 'password']
        ]);
    
        $code = $res->getStatusCode();
    
        if ($code == 400) {
            echo 'bad request' . $this->newLine;
        }
    }

    function updatePost($id, $post) {
        $endpoint = '/posts/' . $id;

        $res = $this->client->put($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($post),
            'auth' => ['user', 'password']
        ]);
    
        $code = $res->getStatusCode();
    
        if ($code == 400) {
            echo 'bad request' . $this->newLine;
        }
    }

    function deletePost($id) {
        $endpoint = '/posts/' . $id;

        $res = $this->client->request('DELETE', $endpoint, [
            'auth' => ['user', 'password']
        ]);
    
        $code = $res->getStatusCode();
    
        if ($code == 404) {
            echo 'Not found post with id: ' . $id . $this->newLine;
        } else if ($code == 400) {
            echo 'Bad request' . $this->newLine;
        }
    }

}

$idsArray = [1,2,34,54,6,7,9,8];
$posts = [
    ['title' => 'title', 'content' => 'content', 'author' => 'author'],
    ['title' => 'заголовок', 'content' => 'контент', 'author' => 'автор'],
    ['title' => '1заголовок3', 'content' => '1контент', 'author' => '3автор'],
    ['title' => '', 'content' => '435345', 'author' => '234234']
];

$newPosts = [
    ['title' => 'new title', 'content' => 'new content', 'author' => 'new Author'],
    ['title' => 'test', 'content' => 'test', 'author' => 'tstr'],
    ['title' => '893423894', 'content' => '7834content445', 'author' => '      author          '],
    ['title' => '', 'content' => '435345', 'author' => '2342                                  34']
];


// Create instance of TestApi
$testApi = new TestApi($idsArray, $posts, $newPosts, 5001);

// Get /posts
$testApi->getPosts();

// GET /posts/:id
for ($x = 0; $x < count($idsArray); $x++) {
    $testApi->getPostById($idsArray[$x]);
}

// POST /posts
for ($x = 0; $x < count($posts); $x++) {
    $testApi->createPost($posts[$x]);
}

// PUT /posts/:id
for ($x = 0; $x < count($newPosts); $x++) {
    $testApi->updatePost($x, $newPosts[$x]);
}

// DELETE /posts/:id
for ($x = 0; $x < count($idsArray); $x++) {
    $testApi->deletePost($idsArray[$x]);
}

?>