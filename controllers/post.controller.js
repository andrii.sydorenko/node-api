const {
  getPostsModel,
  getPostByIdModel,
  createPostModel,
  updatePostModel,
  deletePostModel,
  getPostsCountModel
} = require('../models/post.model');

const ValidateErrors = require('../helpers/validateErrors.helper');

exports.getPosts = async (req, res) => {
  const errors = ValidateErrors.getErrors(req);

  if (errors) {
    return res.status(400).json(errors);
  }

  const { sort_by = 'id', limit = 10, offset = 0, order_by = 'asc' } = req.query;

  const postsCount = await getPostsCountModel();
  const posts = await getPostsModel(sort_by, limit, offset, order_by);

  res.status(200).json({ success: true, posts, ...postsCount });
};

exports.getPostById = async (req, res) => {
  const errors = ValidateErrors.getErrors(req);

  if (errors) {
    return res.status(400).json(errors);
  }

  const id = parseInt(req.params.id);
  const post = await getPostByIdModel(id);

  if (!post) {
    return res
      .status(404)
      .json({ message: `There is no any post with id: ${id}` });
  }
  res.status(200).json({ success: true, post });
};

exports.createPost = async (req, res) => {
  const errors = ValidateErrors.getErrors(req);

  if (errors) {
    return res.status(400).json(errors);
  }

  const post = await createPostModel(req.body);

  res.status(201).json({ success: true, post });
};

exports.updatePost = async (req, res) => {
  const errors = ValidateErrors.getErrors(req);

  if (errors) {
    return res.status(400).json(errors);
  }

  const id = parseInt(req.params.id);
  const post = await getPostByIdModel(id);

  if (!post) {
    return res
      .status(404)
      .json({ message: `There is no any post with id: ${id}` });
  }

  const result = await updatePostModel(id, req.body, post);
  const updatedPost = {
    id,
    title: result[0],
    content: result[1],
    author: result[2],
    creation_date: result[3]
  };
  res.status(200).json({ success: true, post: updatedPost });
};

exports.deletePost = async (req, res) => {
  const errors = ValidateErrors.getErrors(req);

  if (errors) {
    return res.status(400).json(errors);
  }

  const id = parseInt(req.params.id);
  const post = await getPostByIdModel(id);

  if (!post) {
    return res
      .status(404)
      .json({ message: `There is no any post with id: ${id}` });
  }

  deletePostModel(id);

  res.status(204).json();
};
