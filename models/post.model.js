const pool = require('../config/db.config');

const getPostsModel = async (order_by = 'id', limit = 10, offset = 0, sort_by = 'asc') => {
  const results = await pool.query(
    `SELECT * FROM posts ORDER BY ${order_by} ${sort_by} LIMIT $1 OFFSET $2`,
    [limit, offset]
  );
  return results.rows;
};

const getPostsCountModel = async () => {
  const results = await pool.query(
    'SELECT COUNT(*) as posts_count  FROM posts'
  );
  return results.rows[0];
};

const getPostByIdModel = async id => {
  const results = await pool.query('SELECT * FROM posts WHERE id = $1', [id]);
  return results.rows[0];
};

const createPostModel = async params => {
  const { title, content, author } = params;

  const results = await pool.query(
    'INSERT INTO posts (title, content, author) VALUES ($1, $2, $3) RETURNING id, creation_date',
    [title, content, author]
  );

  const { id, creation_date } = results.rows[0];
  const post = {
    id,
    title,
    content,
    author,
    creation_date
  };

  return post;
};

const updatePostModel = async (id, params, post) => {
  const { title, content, author } = params;

  const queryParams = [
    title || post.title,
    content || post.content,
    author || post.author
  ];

  const results = await pool.query(
    'UPDATE posts SET title = $1, content = $2, author = $3 WHERE id = $4 RETURNING creation_date',
    [...queryParams, id]
  );

  queryParams.push(results.rows[0].creation_date);

  return queryParams;
};

const deletePostModel = async id => {
  await pool.query('DELETE FROM posts WHERE id = $1', [id]);
};

module.exports = {
  getPostsModel,
  getPostByIdModel,
  createPostModel,
  updatePostModel,
  deletePostModel,
  getPostsCountModel
};
