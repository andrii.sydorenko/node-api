const { HOST } = process.env;

const url = HOST;

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'REST API EXPRESS',
    version: '1.0.0',
    description: 'This is a REST API application made with Express.',
    headers: {},
    license: {
      name: 'Licensed Under MIT',
      url: 'https://spdx.org/licenses/MIT.html'
    }
  },
  servers: [
    {
      url,
      description: 'Development server'
    }
  ]
};
exports.options = {
  swaggerDefinition,
  apis: ['./routes/*.js']
};
