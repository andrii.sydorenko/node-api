const { validationResult } = require('express-validator');

class ValidateErrors {
  static errorFormatter({ msg, param }) {
    const format = { [param]: { msg } };
    return format;
  }

  static getErrors(req) {
    const errors = validationResult(req).formatWith(this.errorFormatter);
    if (!errors.isEmpty()) {
      return { errors: errors.array() };
    }
  }
}

module.exports = ValidateErrors;
