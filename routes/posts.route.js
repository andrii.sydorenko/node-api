const { Router } = require('express');
const { param, body, query } = require('express-validator');

const {
  getPosts,
  getPostById,
  createPost,
  updatePost,
  deletePost
} = require('../controllers/post.controller');

const excludeSpecialCharacters = /^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/;
const isNumber = /^[0-9]*$/;

/**
 * @swagger
 * components:
 *   schemas:
 *     Post:
 *       type: object
 *       properties:
 *         id:
 *          type: integer
 *          description: The post ID.
 *          example: 1
 *         title:
 *          type: string
 *          description: The post's title.
 *          example: New javascript framework
 *         content:
 *          type: string
 *          description: The post's content.
 *          example: content about new js framework
 *         author:
 *          type: string,
 *          description: The post's author.
 *          example: Mark Twein
 *         creation_date:
 *          type: string
 *          description: date of post's creation
 *          example: 2021-06-06
 */

const router = Router();

/**
 * @swagger
 * /posts:
 *   get:
 *     summary: Retrieve a list of posts.
 *     description: Can be used to get all post from database
 *     parameters:
 *       - in: query
 *         name: offset
 *         schema:
 *           type: integer
 *         description: The number of items to skip before starting to collect the result set
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *         description: The numbers of items to return
 *       - in: query
 *         name: sort_by
 *         schema:
 *           type: string
 *         description: Sort posts by id, title, content, author, creation_date
 *       - in: query
 *         name: order_by
 *         schema:
 *           type: string
 *         description: Order posts by ASC or DESC
 *     responses:
 *       200:
 *         description: A list of post.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Post'
 */

router.get(
  '/posts',
  [
    query('sort_by')
      .optional()
      .matches(/\bid\b|\btitle\b|\bcontent\b|\bauthor\b|\bcreation_date\b/)
      .withMessage(
        'Wrong parameter value you can use: id, title, content, author, creation_date'
      ),
    query('order_by')
      .optional()
      .matches(/\basc\b|\bdesc\b/)
      .withMessage('Wrong parameter value you can use: asc or desc'),
    query('limit')
      .optional()
      .matches(isNumber)
      .withMessage('Number is required'),
    query('offset')
      .optional()
      .matches(isNumber)
      .withMessage('Number is required')
  ],
  getPosts
);

/**
 * @swagger
 * /posts/{id}:
 *   get:
 *     summary: Retrieve an exising post by id
 *     description: Can be used to get an exising post by id
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the post to retrieve.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: A list of posts.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                         description: The post ID.
 *                         example: 0
 *                       title:
 *                         type: string
 *                         description: The post's title.
 *                         example: New javascript framework
 *                       content:
 *                         type: string
 *                         description: The post's content.
 *                         example: content about new js framework
 *                       author:
 *                        type: string,
 *                        description: The post's author.
 *                        example: Mark Twein
 *                       creation_date:
 *                        type: string
 *                        description: date of post's creation
 *                        example: 2021-06-06
 */

router.get(
  '/posts/:id',
  param('id')
    .matches(isNumber)
    .withMessage('Id must be numeric type, like 1 or 34'),
  getPostById
);

/**
 * @swagger
 * /posts:
 *   post:
 *     summary: Create new post
 *     description: Can be used to create new post
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                type: string
 *                description: The post's title.
 *                example: New javascript framework
 *               content:
 *                type: string
 *                description: The post's content.
 *                example: content about new js framework
 *               author:
 *                type: string,
 *                description: The post's author.
 *                example: Mark Twein
 *     responses:
 *       200:
 *         description: Create new post
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       title:
 *                         type: string
 *                         description: The post's title.
 *                         example: New javascript framework
 *                       content:
 *                         type: string
 *                         description: The post's content.
 *                         example: content about new js framework
 *                       author:
 *                        type: string,
 *                        description: The post's author.
 *                        example: Mark Twein
 */

router.post(
  '/posts',
  [
    body(
      'title',
      'Title is required and must be no more than 30 characters also it must contains only alpha-numeric values'
    )
      .trim()
      .isLength({ min: 1, max: 30 })
      .matches(excludeSpecialCharacters),
    body(
      'content',
      'Content is required and must be no more than 500 characters'
    )
      .trim()
      .isLength({ min: 1, max: 500 }),
    body(
      'author',
      'Author is required and must be no more than 30 characters also it must contains only alpha-numeric values'
    )
      .trim()
      .isLength({ min: 1, max: 30 })
      .matches(excludeSpecialCharacters)
  ],
  createPost
);

/**
 * @swagger
 * /posts/{id}:
 *   put:
 *     summary: Update post
 *     description: Can be used to update an existing post
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the post to retrieve.
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                type: string
 *                description: The post's title.
 *                example: New javascript framework
 *               content:
 *                type: string
 *                description: The post's content.
 *                example: content about new js framework
 *               author:
 *                type: string,
 *                description: The post's author.
 *                example: Mark Twein
 *     responses:
 *       200:
 *         description: Update post
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       title:
 *                         type: string
 *                         description: The post's title.
 *                         example: New javascript framework
 *                       content:
 *                         type: string
 *                         description: The post's content.
 *                         example: content about new js framework
 *                       author:
 *                        type: string,
 *                        description: The post's author.
 *                        example: Mark Twein
 */

router.put(
  '/posts/:id',
  param('id')
    .matches(isNumber)
    .withMessage('Id must be numeric type for example it could be 54 or 32'),
  [
    body(
      'title',
      'Title must be shorter than 30 characters also it must contains only alpha-numeric values'
    )
      .trim()
      .isLength({
        max: 30
      })
      .matches(excludeSpecialCharacters),
    body('content', 'Content must be shorter than 500 characters')
      .isLength({
        max: 500
      })
      .trim(),
    body(
      'author',
      'Author must be shorter than 30 characters also it must contains only alpha-numeric values'
    )
      .trim()
      .isLength({
        max: 30
      })
      .matches(excludeSpecialCharacters)
  ],
  updatePost
);

/**
 * @swagger
 * /posts/{id}:
 *   delete:
 *     summary: Delete post
 *     description: Can be used to delete an existing post
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the post to retrieve.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: Delete post
 */

router.delete(
  '/posts/:id',
  param('id')
    .matches(isNumber)
    .withMessage('Id must be numeric type, 20 for example'),
  deletePost
);

module.exports = router;
