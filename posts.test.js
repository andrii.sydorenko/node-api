const supertest = require('supertest');
const app = require('./app');
const chai = require('chai');
const expect = chai.expect;

describe('Get all posts', function () {
  it('it should has status code 200 and have posts object', function (done) {
    supertest(app)
      .get('/posts')
      .auth('user', 'password')
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function (err, res) {
        if (err) done(err);
        expect(res.body).to.have.property('posts').to.that.instanceOf(Object);
        done();
      });
  });
});

describe('Get post by id with wrong id format', function () {
  it('it should has status code 400', function (done) {
    supertest(app)
      .get('/posts/NaN')
      .auth('user', 'password')
      .expect(400)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('Get post by id with unexisting id', function () {
  it('it should has status code 404', function (done) {
    supertest(app)
      .get('/posts/99999999')
      .auth('user', 'password')
      .expect(404)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('POST /posts', function () {
  it('Add new post and check response body', function (done) {
    supertest(app)
      .post('/posts')
      .auth('user', 'password')
      .send({ title: 'title', content: 'some content', author: 'author' })
      .expect(201)
      .end(function (err, res) {
        if (err) done(err);
        expect(res.body)
          .to.have.property('post')
          .to.that.includes.all.keys([
            'id',
            'title',
            'content',
            'author',
            'creation_date'
          ]);
        done();
      });
  });
});

describe('POST /posts', function () {
  it('title and author must be no more than 30 symbols and not contain special characters, content must be no more than 500 symbols', function (done) {
    supertest(app)
      .post('/posts')
      .auth('user', 'password')
      .send({ title: 't|tle', content: 'some content', author: '@uth0r' })
      .expect(400)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('PUT /posts', function () {
  it('Update post by id', function (done) {
    supertest(app)
      .put('/posts/99999999')
      .auth('user', 'password')
      .send({ title: 'title', content: 'some content', author: 'auth0r' })
      .expect(404)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('Delete post by id with unexisted id', function () {
  it('it should has status code 404', function (done) {
    supertest(app)
      .get('/posts/9999999')
      .auth('user', 'password')
      .expect(404)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('Test if fields login and password are empty', function () {
  it('it should has status code 401 -- authorization needed', function (done) {
    supertest(app)
      .get('/posts')
      .expect(401)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('Test wrong password', function () {
  it('it should has status code 401 -- wrong password', function (done) {
    supertest(app)
      .get('/posts')
      .auth('user', 'passwor')
      .expect(401)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe('Test wrong login', function () {
  it('it should has status code 401 -- wrong login', function (done) {
    supertest(app)
      .get('/posts')
      .auth('use', 'password')
      .expect(401)
      .end(function (err, res) {
        if (err) done(err);
        done();
      });
  });
});
