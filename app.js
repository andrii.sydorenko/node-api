const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
require('dotenv').config();
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const postRouter = require('./routes/posts.route');

const auth = require('./middlewares/auth.middleware');
const { options } = require('./config/swagger.config');

const app = express();

const PORT = process.env.PORT || 3000;
const { HOST } = process.env;

app.use(morgan('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(auth);

app.use(postRouter);

app.set('Access-Control-Allow-Origin', '*');
app.set('Access-Control-Allow-Origin', HOST);
app.set('Access-Control-Allow-Credentials', 'true');

const swaggerSpec = swaggerJSDoc(options);

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(PORT, () => {
  console.log(`App running on ${HOST}:${PORT}`);
});

module.exports = app;
